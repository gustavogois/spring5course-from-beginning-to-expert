# Building a Spring Boot Web App

[Start Branch](https://gitlab.com/gustavogois/sbte_spring_boot_web_app/tree/master)

## Creating the project

- Go to [Spring initializer](https://start.spring.io/)
- I used Maven and Spring boot 2.1.3. I also added the following dependencies: 
    - Web, JPA, H2 (in-memory database), Thymeleaf, Actuator
- Extract the zip file and open the folder through your fvorite IDE

Some important points to highlight:
- You don't need to have Maven installed. See ```mvnw```.
- In ```pom.xml```, the use of spring-boot-starter-parent: *curated release*.
- See how many dependencies Spring Boot downloaded for you. See, for example, the dependencies below Spring Boot Starter Data JPA.

## Running the Application

Open a terminal, go to a project directory and type:

```
./mvnw spring-boot:run
```

Tomcat will execute on 8080 port.

[See my Result](https://gitlab.com/gustavogois/spring-boot-web-app/tree/master)

## Using JPA Entities

[Initial branch for this section](https://gitlab.com/gustavogois/spring-course-spring-boot-web-app/tree/using_jpa_and_hibernate)

- See the model classes: ```Author``` and ```Book```

- To do: ```@Entity```, ```@Id``` and ```@manyToMany```

- You can start the application from your IDE, from ```SpringBootWebAppApplication```class.
    - See some Hibernate annotations.

- Configure H2 database, in ```application.properties```
    - ```spring.h2.console.enabled=true```

- Restart the application and see on ```http://localhost:8080/h2-console```
    - Click on ```connect```

- Realize that hibernate created ```AUTHOR_BOOKS```and ```BOOK_AUTHORS```. Let's fix this. Adjust the many to many configurations in ``Àuthor``` and ```Book```.