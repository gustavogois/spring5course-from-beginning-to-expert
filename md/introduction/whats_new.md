# What's new?

At a high level, features of Spring Framework 5.0 can be categorized into:

- **JDK baseline update** - The entire Spring framework 5.0 codebase runs on Java 8. Therefore, Java 8 is the minimum requirement to work on Spring Framework 5.0. Originally, Spring Framework 5.0 was expected to release on Java 9. However, with the Java 9 release running 18 months + behind, the Spring team decided to decouple the Spring Framework 5.0 release from Java 9.
- **Core framework revision** - The core Spring Framework 5.0 has been revised to utilize the new features introduced in Java 8. The key ones are:
    - Based on Java 8 reflection enhancements, method parameters in Spring Framework 5.0 can be efficiently accessed.
    - Core Spring interfaces now provide selective declarations built on Java 8 default methods.
    - @Nullable and @NotNull annotations to explicitly mark nullable arguments and return values. This enables dealing null values at compile time rather than throwing NullPointerExceptions at runtime.
    - On the logging front, Spring Framework 5.0 comes out of the box with Commons Logging bridge module, named spring-jcl instead of the standard Commons Logging. Also, this new version will auto detect Log4j 2.x, SLF4J, JUL ( java.util.logging) without any extra bridges.
- **Core container updates**
    - Spring Framework 5.0 now supports candidate component index as an alternative to classpath scanning. For larger projects, it makes a significant difference. The startup time for our applications will be reduced significantly, for example.
    - Now  @Nullable annotations can also be used as indicators for optional injection points. Using  @Nullableimposes an obligation on the consumers that they must prepare for a value to be null. Prior to this release, the only way to accomplish this is through Android’s Nullable, Checker Framework’s Nullable, and JSR 305’s Nullable.
    - Some other new and enhanced features from the release note are:
        - Implementation of functional programming style in GenericApplicationContextandAnnotationConfigApplicationContext
        - Consistent detection of transaction, caching, async annotations on interface methods.
        - XML configuration namespaces streamlined towards unversioned schemas.
- **Functional programming with Kotlin** - With Kotlin support, developers can dive into functional Spring programming, in particular for functional Web endpoints and bean registration.
- **Reactive Programming Model** - An exciting feature in this Spring release is the new reactive stack Web framework.
Being fully reactive and non-blocking, this Spring Framework 5.0 is suitable for event-loop style processing that can scale with a small number of threads.

    [Reactive Streams](https://springframework.guru/reactive-streams-in-java/) is an API specification developed by engineers from Netflix, Pivotal, Typesafe, Red Hat, Oracle, Twitter, and Spray.io. This provides a common API for reactive programming implementations to implement. Much like JPA for Hibernate. Where JPA is the API, and Hibernate is the implementation.
    The Reactive Streams API is officially part of Java 9. In Java 8, you will need to include a dependency for the Reactive Streams API specification.
- **Testing improvements** - Spring Framework 5.0 fully supports Junit 5 Jupiter to write tests and extensions in JUnit 5. In addition to providing a programming and extension model, the Jupiter sub-project provides a test engine to run Jupiter based tests on Spring.

    In addition, Spring Framework 5 provides support for parallel test execution in Spring TestContext Framework. For the reactive programming model, spring-test now includes WebTestClient for integrating testing support for Spring WebFlux. The new WebTestClient, similar to MockMvc does not need a running server. Using a mock request and response, WebTestClient can bind directly to the WebFlux server infrastructure.
- **Library support** - Spring Framework 5.0 now supports the following upgraded library versions:

    - Jackson 2.6+
    - EhCache 2.10+ / 3.0 GA
    - Hibernate 5.0+
    - JDBC 4.0+
    - XmlUnit 2.x+
    - OkHttp 3.x+
    - Netty 4.1+
- **Discontinued support**
    - At the API level, Spring Framework 5.0 has discontinued support for the following packages:
        - beans.factory.access
        - jdbc.support.nativejdbc
        - mock.staticmock of the spring-aspects module.
        - web.view.tiles2M. Now Tiles 3 is the minimum requirement.
        - orm.hibernate3 and orm.hibernate4. Now, Hibernate 5 is the supported framework.

    - Spring Framework 5.0 has also discontinued support for the following libraries:
        - Portlet
        - Velocity
        - JasperReports
        - XMLBeans
        - JDO
        - Guava

    If you are using any of the preceding packages, it is recommended to stay on Spring Framework 4.3.x.