# Introduction

Spring Framework 5.0 is the first major release of the Spring Framework since version 4 was released in December of 2013. [Juergen Hoeller](https://spring.io/team/jhoeller), Spring Framework project lead announced the release of the first Spring Framework 5.0 milestone (5.0 M1) on 28 July 2016.

## [What's new in Spring 5?](./introduction/whats_new.md)

## Building a Spring Boot Web App

